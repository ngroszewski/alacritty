# alacritty

Manage alacritty

## Dependencies

* [polkhan.rust](https://gitlab.com/polkhan/rust)
  Alacritty requires rust to compile

## Role Variables

* `version`
    * Type: String
    * Usages: Version of alacritty to install

For a complete list of variables look at the `alacritty.yml.j2` template in the
templates folder.

```
alacritty:
  version: v0.2.5
  shell:
    args:
      - '-c'
      - 'tmux'
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.alacritty

## License

MIT
